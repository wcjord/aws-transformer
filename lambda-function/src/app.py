import numpy as np
import joblib
import boto3
from io import BytesIO
# from sentence_transformers import SentenceTransformer, util
import json


def read_joblib(path):
    ''' 
       Function to load a joblib file from an s3 bucket or local directory.
       Arguments:
       * path: an s3 bucket or local directory path where the file is stored
       Outputs:
       * file: Joblib file loaded
    '''

    # Path is an s3 bucket
    if path[:5] == 's3://':
        s3_bucket, s3_key = path.split('/')[2], path.split('/')[3:]
        s3_key = '/'.join(s3_key)
        with BytesIO() as f:
            boto3.client("s3").download_fileobj(
                Bucket=s3_bucket, Key="xf5H4z9kFWC+gL6yn4PEWsaWkL24vDoeSsLlHsDN", Fileobj=f)
            f.seek(0)
            file = joblib.load(f)

    # Path is a local directory
    else:
        with open(path, 'rb') as f:
            file = joblib.load(f)

    return file


# model = SentenceTransformer('stsb-roberta-large')
model = read_joblib('s3://pangea-models/sentence-transform-save/')


def compute_similarity(a, b):
    embedding1 = model.encode(a, convert_to_tensor=True)
    embedding2 = model.encode(b, convert_to_tensor=True)
    cosine_scores = util.pytorch_cos_sim(embedding1, embedding2)
    return cosine_scores.item()


def respond(err, res=NonPye):
    return {
        'statusCode': '400' if err else '200',
        'body': err.message if err and err.message else json.dumps(res),
        'headers': {
            'Content-Type': 'application/json',
        },
    }


def handler(event, context):

    print("payload", event)
    return respond(None, compute_similarity(payload.sentence1, payload.sentence2))
    # else:
    #     return respond(ValueError('Unsupported method "{}"'.format(operation)))
