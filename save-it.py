import numpy as np
from sentence_transformers import SentenceTransformer, util

model = SentenceTransformer('stsb-roberta-large')

model.save("./sentence-transform-save")
