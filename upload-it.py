import boto3
from io import BytesIO


def write_joblib(file, path):
    ''' 
       Function to write a joblib file to an s3 bucket or local directory.
       Arguments:
       * file: The file that you want to save 
       * path: an s3 bucket or local directory path. 
    '''

    # Path is an s3 bucket
    if path[:5] == 's3://':
        s3_bucket, s3_key = path.split('/')[2], path.split('/')[3:]
        s3_key = '/'.join(s3_key)
        with BytesIO() as f:
            joblib.dump(file, f)
            f.seek(0)
            boto3.client("s3").upload_fileobj(
                Bucket=s3_bucket, Key=s3_key, Fileobj=f)

    # Path is a local directory
    else:
        with open(path, 'wb') as f:
            joblib.dump(file, f)


write_joblib("./sentence-transform-save", 's3://bucket_name/mdl_dict.joblib')
